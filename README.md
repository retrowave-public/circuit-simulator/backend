# Backend

## Introduction
This is a backend of our circuit simulator.

This circuits we want to simulate consist out of following components: 
- Resistor
- Voltage source
- Wire

## Table of resistance of different materials

| Material | Resistance | 
| -------- | ---------- | 
| Water | 18.2 **mOhm** |
| Copper| 1.68 x 10-8 **Ohm**|