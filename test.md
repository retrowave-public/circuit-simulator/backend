class Wheel(object):
    def moveForward(self):
        pass

class BicycleFrame(object):
    def moveForward(self):
        pass

class Bicycle(object):
    wheel_1:Wheel
    wheel_2:Wheel
    frame:BicycleFrame

    def ride(self):
        self.wheel_1.moveForward()
        self.wheel_2.moveForward()
        self.frame.moveForward()
