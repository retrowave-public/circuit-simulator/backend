# Circuit File Format

## Example:

```
N1-R1(5k)-N2
```

In this example, ```N``` shows the “node” where the connection of 2 elements occurs, a number or letter that shows the designation (index) of this element. ```R``` shows “resistor” for a resistor you can set the value of its resistance using numbers in parentheses and letters indicating the multiple of the number (in the example “k” - kilo, multiplies the number in front of it by 1000), the rules for its “indexing” work the same as for the ```N``` element. ```V``` stands for "Voltage source" which works as a voltage source, to record the output voltage the same method is used as in the case of ```R``` (Resistor), example:

```
N1-V1(5)-N2
```
indexing occurs in the same way as in other cases. And the last element is ```W``` which stands for "wire" this element takes values ​​and passes them on through the "Circuit", indexing works in the same way as for previous elements.

## Regular Expression

```
/^N([\w\d]+)-([VRW])([\w\d]+)\((\d+(\.\d+)?)([kunmMGT]?)\)?-N([\w\d]+)$/gm
```