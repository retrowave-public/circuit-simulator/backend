import circuit_description_file_parser as cdfp
import circuit_description_line_parser as cdlp

if __name__ == '__main__':
    # Open test-001.txt file
    circuit_description_file_content:str = None

    with open('./examples/test-001.txt', 'r') as circuit_description_file_handler:
        circuit_description_file_content = circuit_description_file_handler.read()

    # Parse file
    file_parser:cdfp.CircuitDescriptionFileParser = cdfp.CircuitDescriptionFileParser(circuit_description_file_content)
    decription_lines:list[cdlp.CircuitDescriptionLine] = file_parser.parse()

    # Check if resistor "4" has 1k Ohm
    found_resistor_4_with_1k_ohm = False
    for dl in decription_lines:
        print(dl)
        if (dl.get_element_name() == "4") and (dl.get_element_type() == "R") and (dl.get_parameter_value() == "1") and (dl.get_parameter_suffix() == "k"):
            found_resistor_4_with_1k_ohm = True
    
    assert (found_resistor_4_with_1k_ohm == True), "The parser was not able to find the resistor 4 with 1k Ohm!"

    # Check if wire x is connected with node 3 (left) and node 2 (right)
    found_wire_x_connected_with_node_3_and_2 = False
    for dl in decription_lines:
        if (dl.get_element_name() == "x") and (dl.get_element_type() == "W") and (dl.get_left_node_name() == "3") and (dl.get_right_node_name() == "2"):
            found_wire_x_connected_with_node_3_and_2 = True
    
    assert (found_wire_x_connected_with_node_3_and_2 == True), "The parser was not able to find the wire x connected to node 3 on the left, and node 2 on the right!"