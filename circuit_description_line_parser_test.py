import circuit_description_line_parser as cdlp

if __name__ == '__main__':
    # Parse a line with a resistor of 1k ohm
    parsed_line_1:cdlp.CircuitDescriptionLine = cdlp.parse_line("N1-Rtest(1k)-N2")
    left_node_name_1:str = parsed_line_1.get_left_node_name()
    assert (left_node_name_1 == "1"), "The parser got a false name of the left node!"

    # Parse a line with a resistor of 1k ohm
    parsed_line_2:cdlp.CircuitDescriptionLine = cdlp.parse_line("N2-R2(1k)-Nabc")
    right_node_name_2:str = parsed_line_2.get_right_node_name()
    assert (right_node_name_2 == "abc"), "The parser got a false name of the left node!"

    print("All test passed!")
