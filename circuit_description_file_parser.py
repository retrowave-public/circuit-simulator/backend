import circuit_description_line_parser as cdlp

class CircuitDescriptionFileParser(object):
    file_content:str

    def __init__(self, file_content:str):
        self.file_content = file_content

    def parse(self) -> list[cdlp.CircuitDescriptionLine]:
        return list()