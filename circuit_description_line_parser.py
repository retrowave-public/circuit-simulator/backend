import re
from typing import Optional
import sys

class CircuitDescriptionLine(object):
    left_node_name:str
    right_node_name:str
    element_name:str
    component_type:str
    parameter_value:str
    parameter_value_suffix:str

    def __init__(self, lnn:str, rnn:str, en:str, ct:str, pv:str, pvs:str):
        self.left_node_name = lnn
        self.right_node_name = rnn
        self.element_name = en
        self.component_type = ct
        self.parameter_value = pv
        self.parameter_value_suffix = pvs
 
    def get_left_node_name(self) -> str:
        return self.left_node_name
    
    def get_right_node_name(self) -> str:
        return self.right_node_name
    
    def get_element_name(self) -> str:
        return self.element_name
    
    def get_element_type(self) -> str:
        return self.component_type
    
    def get_parameter_value(self) -> str:
        return self.parameter_value
    
    def get_parameter_suffix(self) -> str:
        return self.parameter_value_suffix

def parse_line(input_line:str) -> Optional[CircuitDescriptionLine]:
    regex = r"^N([\w\d]+)-([VRW])([\w\d]+)\((\d+(\.\d+)?)([kunmMGT]?)\)?-N([\w\d]+)$"
    
    matches = re.finditer(regex, input_line)

    result = None

    for m in matches:
        mg = m.groups()
        if len(mg) == 7:
            left_node_name = mg[0]
            component_type = mg[1]
            component_name = mg[2]
            parameter_value = mg[3]
            parameter_value_suffix = mg[5]
            right_node_name = mg[6]
            result = CircuitDescriptionLine(left_node_name,right_node_name,component_name,component_type,parameter_value,parameter_value_suffix)

    return result