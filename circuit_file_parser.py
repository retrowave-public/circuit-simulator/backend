import circuit_description_file_parser as cdfp
import circuit_description_line_parser as cdlp

# Circuit file parser
# Transform the text file with a circuit description into a convenient format for further processing


if __name__ == '__main__':
    # Open test-001.txt file
    circuit_description_file_content:str = None

    with open('./examples/test-001.txt', 'r') as circuit_description_file_handler:
        circuit_description_file_content = circuit_description_file_handler.read()

    a = 1